package com.app.gittrendingrepo.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}