package com.app.gittrendingrepo.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.app.gittrendingrepo.data.TrendingDataModelItem

//@Dao should either be an interface or an abstract class
//it used to interact with database and it contains the queries
@Dao
interface TrendingDao {

    @Insert
    suspend fun update(trendingGit: List<TrendingDataModelItem>)

    @Query("SELECT * FROM TrendingDataModelItem WHERE name LIKE :search || '%'")
    suspend fun getSearchResult(search: String): List<TrendingDataModelItem>

    @Query("SELECT * FROM TrendingDataModelItem")
    suspend fun getTrendingList(): List<TrendingDataModelItem>

    @Query("SELECT COUNT(*) FROM TrendingDataModelItem")
    suspend fun getCount():Int
}