package com.app.gittrendingrepo.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.app.gittrendingrepo.data.TrendingDataModelItem

@Database(entities = [TrendingDataModelItem::class], version = 1, exportSchema = false)
abstract class TrendingDatabase: RoomDatabase(){

    abstract fun getTrendingDao(): TrendingDao

    companion object {

        //volatile-> memory visibility
        // that means the instance of this database is available immediately to all other running threads
        //here we are creating the instance of our database
        @Volatile
        private var INSTANCE: TrendingDatabase? = null

        fun getInstance(context: Context): TrendingDatabase {
            //synchronized method will be protected from concurrent execution
            // by multiple threads by the monitor of the instance
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        TrendingDatabase::class.java,
                        "git_trending_list"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }

}