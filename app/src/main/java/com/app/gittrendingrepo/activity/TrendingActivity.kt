package com.app.gittrendingrepo.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.app.gittrendingrepo.R
import com.app.gittrendingrepo.adapter.TrendingListAdapter
import com.app.gittrendingrepo.common.CheckingConnection
import com.app.gittrendingrepo.common.Status
import com.app.gittrendingrepo.common.isConnected
import com.app.gittrendingrepo.databinding.ActivityTrendingBinding
import com.app.gittrendingrepo.utils.hide
import com.app.gittrendingrepo.utils.show
import com.app.gittrendingrepo.utils.snackBar
import com.app.gittrendingrepo.viewmodel.TrendingViewModel
import kotlinx.android.synthetic.main.activity_trending.*

class TrendingActivity : AppCompatActivity() {
    private var viewModel: TrendingViewModel? = null
    private lateinit var networkConnection: CheckingConnection
    private var networkStatus: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    /*
     * This field is used for data binding. Normally, we would have to call findViewById many
     * times to get references to the Views in this Activity. With data binding however, we only
     * need to call DataBindingUtil.setContentView and pass in a Context and a layout, as we do
     * in onCreate of this class. Then, we can access all of the Views in our layout
     * programmatically without cluttering up the code with findViewById.
     */
        val binding: ActivityTrendingBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_trending
        )

        // AndroidViewModelFactory is to pass application context for room database
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(
            TrendingViewModel::class.java
        )
        binding.uiModel = viewModel

        /* creating instance of NetworkConnection to start broadcast receiver
         that extents livedata which is lifecycle aware*/
        networkConnection = CheckingConnection(this)
        networkConnection.observe(this) {
            networkStatus = it
        }
        networkStatus = isConnected

        swipe_refresh.setProgressBackgroundColorSchemeResource(R.color.blue)
        swipe_refresh.setColorSchemeResources(R.color.white)

        swipe_refresh.setOnRefreshListener {
            getTrendingRepository()
        }

        //searchview is used to listen search query
        search_button.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText?.isEmpty()!!) {
                    getOfflineData()
                    return true
                }
                getSearchResult(newText)
                return true
            }

        })

        search_button.setOnSearchClickListener {
            viewModel!!.searchEnabled.set(true)
            swipe_refresh.isEnabled = false
        }

        search_button.setOnCloseListener {
            viewModel!!.searchEnabled.set(false)
            swipe_refresh.isEnabled = true
            false
        }

        back_icon.setOnClickListener {
            search_button.setQuery("", false)
            search_button.isIconified = true
        }

        retry_button.setOnClickListener {
            getTrendingRepository()
        }
        getTrendingRepository()

    }

    override fun onBackPressed() {
        if (!search_button.isIconified) {
            search_button.isIconified = true
        } else {
            super.onBackPressed()
        }
    }

    // this method check the internet connection and request viewmodel for response
    private fun getTrendingRepository() {
        if (networkStatus) {
            viewModel!!.noConnection.set(false)

            getTrendingResult()
        } else {
            viewModel!!.getDatabaseCount().observe(this, {
                if (it) {
                    swipe_refresh.isRefreshing = false
                    viewModel!!.noConnection.set(false)
                    getOfflineData()
                    progress_bar.hide()
                    main_layout.snackBar("No internet connection. Loading offline data...")
                } else {
                    progress_bar.hide()
                    swipe_refresh.isRefreshing = false
                    viewModel!!.noConnection.set(true)
                }

            })

        }
    }

    private fun getTrendingResult() {

         // Observers changes in the TrendingDataModelItem and update recyclerview
        viewModel?.getTrendingResponse()?.observe(this, {
            when (it.status) {
                Status.SUCCESS -> {

                    swipe_refresh.isRefreshing = false
                    trending_recycler_view.apply {
                        adapter = it.data?.let { it1 -> TrendingListAdapter(it1) }
                    }
                    progress_bar.hide()
                }
                Status.ERROR -> {
                    progress_bar.hide()
                    main_layout.snackBar(it.message!!)
                }
                Status.LOADING -> progress_bar.show()
            }
        })

    }

    //Pass search query argument to database and observes the result
    fun getSearchResult(search: String) {

        viewModel?.getSearchResponse(search)?.observe(this, { it ->
            if (it.isEmpty()) {
                viewModel!!.noResult.set(true)
            } else {
                viewModel!!.noResult.set(false)
                trending_recycler_view.apply {
                    adapter = it?.let { it1 -> TrendingListAdapter(it1) }
                }
            }

        })
    }

    //this method is called when no internet connection
    // and it updates recyclerview with data from local database
    fun getOfflineData() {

        viewModel?.getOfflineResponse()?.observe(this, { it ->
            viewModel!!.noResult.set(false)
            trending_recycler_view.apply {
                adapter = it?.let { it1 -> TrendingListAdapter(it1) }
            }
        })
    }
}
