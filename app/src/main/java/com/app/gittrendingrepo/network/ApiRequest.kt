package com.app.gittrendingrepo.network

import com.app.gittrendingrepo.data.TrendingDataModel
import retrofit2.http.GET

interface ApiRequest {

    @GET("repositories")
    suspend fun getTrendingRepos(): TrendingDataModel


}