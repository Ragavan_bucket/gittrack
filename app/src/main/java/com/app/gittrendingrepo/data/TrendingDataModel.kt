package com.app.gittrendingrepo.data

import androidx.room.Entity
import androidx.room.PrimaryKey


class TrendingDataModel : ArrayList<TrendingDataModelItem>()

//entities are the columns fro the room database
//each entity must have at least 1 field annotated with primarykey
@Entity
data class TrendingDataModelItem(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var author: String,
    var avatar: String,
    var currentPeriodStars: Int,
    var description: String,
    var forks: Int,
    var language: String,
    var languageColor: String,
    var name: String,
    var stars: String,
    var url: String
)