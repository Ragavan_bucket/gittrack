package com.app.gittrendingrepo.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.gittrendingrepo.R
import com.app.gittrendingrepo.data.TrendingDataModelItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.adapter_trending.view.*

class TrendingListAdapter(private val trendingList: List<TrendingDataModelItem>) : RecyclerView.Adapter<TrendingListAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val avatarImageView: ImageView = itemView.avatar_image_view
        val authorTextView: TextView = itemView.author_text_view
        val topicTextView: TextView = itemView.topic_text_view
        val descriptionTextView: TextView = itemView.description_text_view
        val languageColorTextView: TextView = itemView.language_color_view
        val languageNameTextView: TextView = itemView.language_name_text_view
        val ratingTextView: TextView = itemView.rating_text_view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_trending,parent,false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.authorTextView.text = trendingList[position].author
        holder.topicTextView.text = trendingList[position].name
        holder.descriptionTextView.text = trendingList[position].description
        holder.languageNameTextView.text = trendingList[position].language
        holder.ratingTextView.text=trendingList[position].stars

        Glide.with(holder.itemView.context)
            .load(trendingList[position].avatar)
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(holder.avatarImageView)

        holder.languageColorTextView.setTextColor(Color.parseColor(trendingList[position].languageColor))

    }

    override fun getItemCount(): Int {
        return trendingList.size
    }
}