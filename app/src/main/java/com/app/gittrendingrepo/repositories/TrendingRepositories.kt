package com.app.gittrendingrepo.repositories

import android.app.Application
import com.app.gittrendingrepo.common.Resource
import com.app.gittrendingrepo.common.ResponseHandler
import com.app.gittrendingrepo.common.Status
import com.app.gittrendingrepo.data.TrendingDataModel
import com.app.gittrendingrepo.data.TrendingDataModelItem
import com.app.gittrendingrepo.database.TrendingDatabase
import com.app.gittrendingrepo.network.ApiRequest
import com.app.gittrendingrepo.network.RetrofitCall

/*this class is a repository class, according to MVVM architecture
this class is responsible for network call and database access*/

class TrendingRepositories(application: Application) {

    private val responseHandler: ResponseHandler = ResponseHandler()
    private val database: TrendingDatabase = TrendingDatabase.getInstance(application)

    //suspend keyword to perform operation in different thread without blocking the main thread
    // in this class network call and database query are performed by this suspend keyword
    suspend fun getTrendingRepo(): Resource<List<TrendingDataModelItem>> {
        val client: ApiRequest = RetrofitCall.retrofitClient
        return try {
            val response = client.getTrendingRepos()
            if (responseHandler.handleSuccess(response).status == Status.SUCCESS) {
                responseHandler.handleSuccess(response).data?.let { setTrendingRepo(it) }
            }
            return responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }


    private suspend fun setTrendingRepo(response: TrendingDataModel) {
        database.clearAllTables()
        database.getTrendingDao().update(response)
    }

    suspend fun getSearchRepo(search: String): List<TrendingDataModelItem> {
        return database.getTrendingDao().getSearchResult(search)
    }

    suspend fun getOfflineRepo(): List<TrendingDataModelItem> {
        return database.getTrendingDao().getTrendingList()
    }

    private suspend fun getCount(): Int {
        return database.getTrendingDao().getCount()
    }

    suspend fun isDbContainValue(): Boolean {
        try {
            if (getCount() > 0) {
                return true
            }
        } catch (e: Exception) {
            return false
        }
        return false
    }

}