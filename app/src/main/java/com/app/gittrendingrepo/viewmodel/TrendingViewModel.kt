package com.app.gittrendingrepo.viewmodel

import android.app.Application
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData
import com.app.gittrendingrepo.common.Resource
import com.app.gittrendingrepo.repositories.TrendingRepositories
import kotlinx.coroutines.Dispatchers

/*
class is designed to store and manage
UI-related data in a lifecycle conscious way
*/

/*
* AndroidViewModel is subclass of ViewModel.
* and is helpful for room database which require context to create instance
*/

class TrendingViewModel(application: Application) : AndroidViewModel(application) {

    /*
    * ObservableBoolean() methods are used to easily observe data for changes.
    *  You don't have to worry about refreshing the UI
    * when the underlying data source changes
    */
    var noConnection: ObservableBoolean = ObservableBoolean()
    var searchEnabled: ObservableBoolean = ObservableBoolean()
    var noResult: ObservableBoolean = ObservableBoolean()

    private val repository: TrendingRepositories = TrendingRepositories(application)

    /*
    * LiveData is lifecycle-aware, meaning it respects the lifecycle of other app components,
    * such as activities, fragments, or services.
    * This awareness ensures LiveData only updates app component observers that are in an active lifecycle state.
    * */

//this function uses liveData(Dispatchers.IO) coroutine to call a suspend function
//and emit the result to activity
    fun getTrendingResponse() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        val trendingResponse = repository.getTrendingRepo()
        emit(trendingResponse)
    }

    fun getOfflineResponse() = liveData(Dispatchers.IO) {
        val trendingResponse = repository.getOfflineRepo()
        emit(trendingResponse)
    }

    fun getSearchResponse(search: String) = liveData(Dispatchers.IO) {
        val trendingResponse = repository.getSearchRepo(search)
        emit(trendingResponse)
    }

    fun getDatabaseCount() = liveData(Dispatchers.IO) {
        val trendingResponse = repository.isDbContainValue()
        emit(trendingResponse)
    }
}

/*
* the binding adapter can take care of calling the setVisibility() method
* to set the visibility changes according to the observables
*/

@BindingAdapter("trueVisibility")
fun setVisibility(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("falseVisibility")
fun makeVisibility(view: View, visible: Boolean) {
    view.visibility = if (visible) View.GONE else View.VISIBLE
}